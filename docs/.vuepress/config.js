module.exports = {
    title: 'Activiti',
    description: 'Activiti study note',
    base: '/activiti-note/',
    themeConfig: {
        sidebar: 
        [
            {
                title: '入门篇',
                collapsable: false,
                children: [
                    {
                        title: '简介',
                        path: '/guide/'
                    },
                    {
                        title: 'UML',
                        path: '/guide/'
                    }
                ]
            }
        ]
    }
}