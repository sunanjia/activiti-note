---
home: true
heroImage: /logo.jpg
heroText: Activiti 学习笔记
tagline: Activiti study note
actionText: 开始学习 →
actionLink: /guide/
footer: MIT Licensed | Copyright © 2018-present
---